'use strict'

let router=require('koa-router')();

let fs=require('fs');

let path=require('path');

function filterPath(){
    let files=fs.readdirSync(__dirname);
    return files.filter(name=>{
        return name.endsWith('.js') && name!=='index.js';
    })
};

function getCurrentPath(files){
    files.forEach(item=>{
        let fullPath=path.join(__dirname,item);
        let usePath=require(fullPath);
        for(let a in usePath){
            let type=usePath[a][0];
            let fn=usePath[a][1];
            if(type=='get'){
                router.get(a,fn);
            }else if(type=='post'){
                router.post(a,fn);
            }
        }

    })
};

module.exports=function (){
    let files=filterPath();
    getCurrentPath(files);

    return router.routes();
}