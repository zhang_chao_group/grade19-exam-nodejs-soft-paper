'use strict'

let index_fn=async(ctx,next)=>{
  ctx.render('hello.html')
};

let users_fn=async(ctx,next)=>{
    let uid=ctx.request.body.username||"";
    let pwd=ctx.request.body.password||"";

    if(uid==='admin'&& pwd==='123'){
        ctx.redirect('/users/sucess');
    }else{
        ctx.redirect('/users/error');
    }

};
let sucess_fn=async(ctx,next)=>{
    ctx.body="<h1>登陆成功！</h1>"
}
let error_fn=async(ctx,next)=>{
    ctx.body="<h1>用户名或者密码错误，请重试!</h1>"
};

module.exports={
    '/':['get',index_fn],
    '/users':['post',users_fn],
    '/users/sucess':['get',sucess_fn],
    '/users/error':['get',error_fn],
}