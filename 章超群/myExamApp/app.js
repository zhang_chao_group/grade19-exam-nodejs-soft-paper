'use strict'

let Koa=require('koa');

let bodyParser=require('koa-bodyparser');

let app=new Koa();

let staticss=require('koa-static');

app.use(bodyParser());

app.use(staticss(__dirname))

let templating=require('./templating');

app.use(templating);

let controller=require('./controllers');

app.use(controller());



let port=3100;

app.listen(port);

console.log(`当前服务器IP:http://127.0.0.1:${port}`);